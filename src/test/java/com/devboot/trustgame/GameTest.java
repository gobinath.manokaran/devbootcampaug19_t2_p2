package com.devboot.trustgame;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldPlayGameForOneRound(){
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, noOfRounds,console);

        when(player1.move()).thenReturn("CO");
        when(player2.move()).thenReturn("CH");
        when(machine.calculateScore("CO","CH")).thenReturn(new int[]{-1,3});
        when(player1.getScore()).thenReturn(-1);
        when(player2.getScore()).thenReturn(3);

        game.play();
        verify(player1).move();
        verify(player2).move();
        verify(machine).calculateScore("CO", "CH");

        verify(player1).updateScore(-1);
        verify(player2).updateScore(3);
        verify(console).println("-1,3");
    }

    @Test
    public void shouldPlayGameForMultipleRounds(){
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        int noOfRounds = 2;
        PrintStream console = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, noOfRounds,console);

        when(player1.move()).thenReturn("CO").thenReturn("CH");
        when(player2.move()).thenReturn("CH").thenReturn("CH");
        when(machine.calculateScore("CO","CH")).thenReturn(new int[]{-1,3});
        when(machine.calculateScore("CH","CH")).thenReturn(new int[]{0,0});

        game.play();
        verify(player1, times(2)).move();
        verify(player2, times(2)).move();
        verify(machine).calculateScore("CO", "CH");
        verify(machine).calculateScore("CH", "CH");

        verify(player1).updateScore(-1);
        verify(player2).updateScore(3);

        verify(player1).updateScore(0);
        verify(player2).updateScore(0);

        when(player1.getScore()).thenReturn(-1).thenReturn(-1);
        when(player2.getScore()).thenReturn(3).thenReturn(3);

//        verify(console,times(2)).println("-1,3");
//        verify(console).println("0,0");
    }

//    @Test
//    public void shouldMakeMoveCooperateByCopyCat() {
//        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
//        Player player1 = mock(Player.class);
//        Player player2 = new Player(copyCatBehavior);
//        Machine machine = mock(Machine.class);
//        when(player1.move()).thenReturn("CH");
//        when(machine.calculateScore(any(), any())).thenReturn(new int[]{2, 2});
//        int noOfRounds = 1
//        Game game = new Game(player1, player2, machine, noOfRounds);
//        game.addObserver(copyCatBehavior);
//        game.play();
//        Assert.assertEquals("CO", player2.move());
//    }

//    @Test
//    @Ignore
//    public void shouldNotifyLastMoveToCopyCats() {
//        CopyCatBehavior copyCatBehavior = mock(CopyCatBehavior.class);
//        Player player1 = mock(Player.class);
//        Player player2 = new Player(copyCatBehavior);
//        Machine machine = mock(Machine.class);
//        when(player1.move()).thenReturn("CH", "CH");
//        when(machine.calculateScore(any(), any())).thenReturn(new int[]{2, 2});
//        int noOfRounds = 2;
//        Game game = new Game(player1, player2, machine, noOfRounds);
//        game.addObserver(copyCatBehavior);
//        game.play();
//        verify(copyCatBehavior, times(2)).update(game, "CH");
//    }
}
