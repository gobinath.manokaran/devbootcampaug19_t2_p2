package com.devboot.trustgame;

import org.junit.Assert;
import org.junit.Test;

public class CopyCatBehaviorTest {

    @Test
    public void shouldReturnCooperateAsFirstMove() {
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior(1);
        Assert.assertEquals("CO", copyCatBehavior.move());
    }
}
