package com.devboot.trustgame;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class CopyCatBehavior implements PlayerBehavior, Observer {
    private String nextMove = "CO";
    private final int idOfPlayerToCopy;

    public CopyCatBehavior(int idOfPlayerToCopy){
        this.idOfPlayerToCopy = idOfPlayerToCopy;
    }
    @Override
    public String move() {
        return nextMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        Notification notification = (Notification) arg;
        this.nextMove = notification.getPlayerIdsAndTheirMoves().get(idOfPlayerToCopy);
    }
}
