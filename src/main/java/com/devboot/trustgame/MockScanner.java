package com.devboot.trustgame;

public class MockScanner implements ScanInput {
    private String input;

    public void setMoveInput(String move){
        this.input = move;
    }

    @Override
    public String getInput(){
        return this.input;
    }
}
