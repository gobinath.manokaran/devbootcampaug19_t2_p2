package com.devboot.trustgame;

import java.util.Map;

public class Notification {
    private final Map<Integer, String> playerIdsAndTheirMoves;

    public Notification(Map<Integer, String> playerIdsAndTheirMoves) {
        this.playerIdsAndTheirMoves = playerIdsAndTheirMoves;
    }

    public Map<Integer, String> getPlayerIdsAndTheirMoves() {
        return playerIdsAndTheirMoves;
    }
}
