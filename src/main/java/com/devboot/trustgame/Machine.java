package com.devboot.trustgame;

public class Machine {
    public int[] calculateScore(String p1Move, String p2Move) {
        if(p1Move.equalsIgnoreCase("CO") && p2Move.equalsIgnoreCase("CO")){
            return new int[] {2,2};
        }else if(p1Move.equalsIgnoreCase("CO") && p2Move.equalsIgnoreCase("CH")){
            return new int[] {-1,3};
        }else if(p1Move.equalsIgnoreCase("CH") && p2Move.equalsIgnoreCase("CO")){
            return new int[] {3,-1};
        }
        return new int[] {0,0};
    }
}
