package com.devboot.trustgame;

import java.util.Observable;
import java.util.Observer;

public class GrudgeBehavior implements PlayerBehavior, Observer {
    private String nextMove = "CO";
    private final int idOfOpponentToWatch;

    public GrudgeBehavior(int idOfOpponentToWatch) {
        this.idOfOpponentToWatch = idOfOpponentToWatch;
    }

    @Override
    public String move() {
        return nextMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        Notification notification = (Notification) arg;
        String opponentMove = notification.getPlayerIdsAndTheirMoves().get(idOfOpponentToWatch);
        if (opponentMove.equals("CH")) {
            this.nextMove = "CH";
            o.deleteObserver(this);
        }
    }
}
