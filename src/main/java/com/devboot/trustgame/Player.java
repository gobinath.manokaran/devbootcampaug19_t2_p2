package com.devboot.trustgame;

public class Player {

    private final PlayerBehavior playerBehavior;
    private int score;
    private final int id;

    public Player(PlayerBehavior playerBehavior, int id) {
        this.playerBehavior = playerBehavior;
        this.id=id;
    }


    public String move() {
        return playerBehavior.move();
    }

    public void updateScore(int currentScore) {
        score += currentScore;
    }

    public int getScore(){
        return this.score;
    }

    public PlayerBehavior getPlayerBehavior() {
        return playerBehavior;
    }

    public Integer getId() {
        return this.id;
    }
}
