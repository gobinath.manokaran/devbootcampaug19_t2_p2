package com.devboot.trustgame;

public class GameMain {

    public static void main(String[] args) {
        Player player1 = new Player(PlayerBehavior.CHEAT_PLAYER_BEHAVIOR,1);
        Player player2 = new Player(PlayerBehavior.COOPERATE_PLAYER_BEHAVIOR,2);
        Machine machine = new Machine();
        Game game = new Game(player1, player2, machine, 2,System.out);
        game.play();
        System.out.println("Final Score:[player1,player2]"+"["+player1.getScore()+","+player2.getScore()+"]");
    }
}
